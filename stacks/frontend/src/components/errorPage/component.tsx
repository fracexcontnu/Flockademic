import './styles.scss';

import * as React from 'react';

import { PageMetadata } from '../pageMetadata/component';

export interface ErrorPageProps {
  title: string;
  url: string;
}

export const ErrorPage: React.SFC<ErrorPageProps> = (props) => {
  return (
    <div>
      <section className="hero is-medium is-warning">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              <PageMetadata
                url={props.url}
                title={props.title}
              />
              {props.title}
            </h1>
          </div>
        </div>
      </section>
      <section className="section">
        <div className="container">
          {props.children}
        </div>
      </section>
    </div>
  );
};
