import './styles.scss';

import * as React from 'react';
import * as ReactGA from 'react-ga';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ThunkDispatch } from 'redux-thunk';

import { getSlugForName } from '../../../../../lib/filters/periodical';
import { Periodical } from '../../../../../lib/interfaces/Periodical';
import { isPeriodicalOwner, isPublic } from '../../../../../lib/utils/periodicals';
import {
  AlertState,
  completeSomething,
  CompleteSomethingAction,
  IgnoreCompleteSomethingAction,
} from '../../ducks/alert';
import { AppState } from '../../ducks/app';
import { getSession } from '../../services/account';
import {
  getPeriodical,
  isPeriodicalSlugAvailable,
  makePeriodicalPublic,
  updatePeriodical,
} from '../../services/periodical';
import { HeaderManager } from '../headerManager/component';
import { OrcidButton } from '../orcidButton/component';
import { PageMetadata } from '../pageMetadata/component';
import { Spinner } from '../spinner/component';

interface JournalManagementPageState {
  error?: JSX.Element;
  hasBeenMadePublic: boolean;
  isBeingMadePublic: boolean;
  isBeingUpdated: boolean;
  isSlugAvailable?: boolean;
  formValues: {
    description: string;
    headline: string;
    name: string;
  };
  periodical?: null | Partial<Periodical>;
}
export type JournalManagementPageRouteProps = RouteComponentProps<JournalManagementPageRouteParams> | undefined;
export interface JournalManagementPageStateProps {
  orcid?: string;
}
export interface JournalManagementPageDispatchProps {
  onUpdate: (successMessage: JSX.Element) => void;
}
export type JournalManagementPageProps =
  JournalManagementPageStateProps & JournalManagementPageDispatchProps;
export interface JournalManagementPageRouteParams { slug: string; }

export class BareJournalManagementPage
  extends React.Component<
  JournalManagementPageProps & JournalManagementPageRouteProps,
  JournalManagementPageState
> {
  public state: JournalManagementPageState = {
    formValues: {
      description: '',
      headline: '',
      name: '',
    },
    hasBeenMadePublic: false,
    isBeingMadePublic: false,
    isBeingUpdated: false,
  };

  constructor(props: JournalManagementPageProps & RouteComponentProps<JournalManagementPageRouteParams>) {
    super(props);

    this.handleFormFieldChange = this.handleFormFieldChange.bind(this);
    this.makePublic = this.makePublic.bind(this);
    this.updateJournal = this.updateJournal.bind(this);
    this.checkSlugAvailability = this.checkSlugAvailability.bind(this);
    this.renderHeaderUploadForm = this.renderHeaderUploadForm.bind(this);
    this.renderSlugAvailability = this.renderSlugAvailability.bind(this);
    this.onHeaderUpload = this.onHeaderUpload.bind(this);
  }

  public async componentDidMount() {
    try {
      const [ periodical, session ] = await Promise.all([
        getPeriodical(this.props.match.params.slug),
        getSession(),
      ]);

      if (isPublic(periodical) && !isPeriodicalOwner(periodical, session)) {
        this.setState({
          error: (
            <>
              You do not have permission to modify this journal's settings. You can either&nbsp;
              <OrcidButton>Log in</OrcidButton> or&nbsp;
              <Link to="/journals/new" title="Start your own journal">start a new journal</Link>.
            </>
          ),
          periodical: null,
        });
      } else {
        this.setState({
          formValues: {
            description: periodical.description || '',
            headline: periodical.headline || '',
            name: periodical.name || '',
          },
          periodical,
        });
      }
    } catch (e) {
      this.setState({ periodical: null });
    }

    // There's something going wrong in my understanding in the next function:
    // the promise inside `checkSlugAvailability` is `try`d and `catch`d, yet
    // TSLint complains about unhandled promises. Since we don't care about whether
    // it succeeded or failed, supress that error:
    // tslint:disable-next-line:no-floating-promises
    this.checkSlugAvailability();
  }

  public render() {
    return (
      <div>
        <section className="hero is-medium is-dark">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">
                Journal settings
                <PageMetadata url={this.props.match.url} title="Journal settings"/>
              </h1>
            </div>
          </div>
        </section>
        {this.getView()}
      </div>
    );
  }

  private getView() {
    if (typeof this.state.periodical === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }
    if (this.state.periodical === null) {
      return (
        <section className="section">
          <div className="container">
            <div className="message is-danger content">
              <div className="message-body">
                This Journal could not be found, or you do not have permission to change its settings.&nbsp;
                Would you like to&nbsp;
                <Link
                  to="/journals/new"
                  title="Start a new journal"
                >
                  start a new one
                </Link>?
              </div>
            </div>
          </div>
        </section>
      );
    }
    if (this.state.hasBeenMadePublic) {
      return <Redirect to={`/journal/${this.state.periodical.identifier}`}/>;
    }

    return this.renderPeriodical();
  }

  private renderPeriodical() {
    return (
      <div className="container">
        {this.renderErrors()}
        <div className="columns">
          <div className="column">
            <section className="section">
              <h3 className="title is-size-4">Journal details</h3>
              <form className="update-form" onSubmit={this.updateJournal}>
                <div className="field">
                  <label htmlFor="periodicalName" className="label">
                    Journal title
                  </label>
                  <div className="control">
                    <input
                      id="periodicalName"
                      placeholder="Journal title"
                      value={this.state.formValues.name}
                      onChange={this.handleFormFieldChange}
                      onBlur={this.checkSlugAvailability}
                      className={`input ${(this.state.isSlugAvailable === false) ? 'is-danger' : ''}`}
                      name="name"
                    />
                  </div>
                  {this.renderSlugAvailability()}
                </div>
                <div className="field">
                  <label htmlFor="periodicalHeadline" className="label">
                    Tagline
                  </label>
                  <div className="control">
                    <input
                      id="periodicalHeadline"
                      placeholder="Tagline"
                      value={this.state.formValues.headline}
                      onChange={this.handleFormFieldChange}
                      className="input"
                      name="headline"
                    />
                  </div>
                </div>
                <div className="field">
                  <label htmlFor="periodicalDescription" className="label">
                    Description
                  </label>
                  <div className="control">
                    <textarea
                      id="periodicalDescription"
                      placeholder="Description"
                      value={this.state.formValues.description}
                      onChange={this.handleFormFieldChange}
                      className="textarea"
                      name="description"
                    />
                  </div>
                </div>
                <div className="field is-grouped is-grouped-right">
                  <div className="control">
                    <Link
                      to={`/journal/${this.props.match.params.slug}`}
                      className="button is-text"
                    >
                      Cancel
                    </Link>
                  </div>
                  <div className="control">
                    <button type="submit" className={`button is-link ${this.state.isBeingUpdated ? 'is-loading' : ''}`}>
                      Save
                    </button>
                  </div>
                </div>
              </form>
            </section>
            {this.renderHeaderUploadForm()}
          </div>
          <div className="column is-one-third-widescreen is-offset-one-fifth-widescreen">
            {this.renderMakePublicForm()}
          </div>
        </div>
      </div>
    );
  }

  private renderErrors() {
    if (!this.state.error) {
      return null;
    }

    return (
      <section className="section">
        <div className="message is-danger">
          <div className="message-body">
            {this.state.error}
          </div>
        </div>
      </section>
    );
  }

  private renderHeaderUploadForm() {
    if (!this.state.periodical || !this.state.periodical.identifier) {
      return null;
    }

    return (
      <section className="section">
        <h3 className="title is-size-4">Header image</h3>
        <HeaderManager
          periodicalId={this.state.periodical.identifier}
          file={this.state.periodical.image}
          onUpload={this.onHeaderUpload}
        />
      </section>
    );
  }

  private renderMakePublicForm() {
    if (!this.state.periodical || this.state.periodical.datePublished) {
      return null;
    }

    return (
      <aside className="section content">
        <form onSubmit={this.makePublic} className="notification publication-form">
          <h3 className="title">This journal is not public yet</h3>
          <p>Only you can view this journal. You can make it public after completing the following steps:</p>
          <ul className="journalProgress">
            <li id="nameTodo" className={this.getTodoClassForName()}>Name your Journal</li>
            {this.getTodoItemForOrcid()}
          </ul>
          <button
            type="submit"
            className={`button is-primary ${this.state.isBeingMadePublic ? 'is-loading' : ''}`}
            disabled={!this.isReadyForPublication()}
          >
            Make public
          </button>
          <hr/>
          <div className="content">
            {this.renderSlug(this.state.periodical.name)}
          </div>
        </form>
      </aside>
    );
  }

  private renderSlug(name?: string) {
    if (!name) {
      return null;
    }

    return <p>The journal will be visible at <samp>{this.renderSlugUrl(name)}</samp>.</p>;
  }

  private renderSlugUrl(name: string) {
    return `${window.location.host}/journal/${getSlugForName(name)}`;
  }

  private async checkSlugAvailability() {
    if (this.state.periodical && this.state.periodical.datePublished) {
      // If the journal is public already, the slug has been set and thus no longer has to be checked.
      return;
    }

    try {
      const isAvailable = await isPeriodicalSlugAvailable(getSlugForName(this.state.formValues.name));

      if (!isAvailable) {
        this.setState({ isSlugAvailable: false });
      }
    } catch (e) {
      // If we can't check availability, that's too bad - we'll see the error when trying to publish.
    }
  }

  private renderSlugAvailability() {
    if (this.state.isSlugAvailable !== false || !this.state.formValues.name) {
      return null;
    }

    return (
      <p className="help is-danger">
        This name results in a journal link of&nbsp;
        <samp>{this.renderSlugUrl(this.state.formValues.name)}</samp>,
        which is already taken. Please try a different name.
      </p>
    );
  }

  private getTodoItemForOrcid() {
    if (this.props.orcid) {
      return (
        <li id="authenticationTodo" className="journalProgress__todo journalProgress__todo--done">
          Create account / sign in
        </li>
      );
    }

    return (
      <li id="authenticationTodo" className="journalProgress__todo">
        <OrcidButton>Create account / sign in</OrcidButton>
      </li>
    );
  }

  private handleFormFieldChange(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
    const target = event.target;
    this.setState((prevState: JournalManagementPageState) => ({
      formValues: {
        ...prevState.formValues,
        [target.name]: target.value,
      },
      isSlugAvailable: (target.name === 'name') ? undefined : prevState.isSlugAvailable,
    }));
  }

  private async updateJournal(event: React.FormEvent<HTMLFormElement>) {
    // TODO: Only allow valid fields when the Journal is already public
    event.preventDefault();
    this.setState({ isBeingUpdated: true });

    ReactGA.event({
      action: 'Update details',
      category: 'Journal Management',
    });

    try {
      const response = await updatePeriodical(this.props.match.params.slug, this.state.formValues);
      this.setState((prevState: JournalManagementPageState) => ({
        isBeingUpdated: false,
        periodical: {
          ...prevState.periodical,
          ...response.result,
        },
      }));
      this.props.onUpdate(
        <span>
          Saved!&nbsp;
          <Link
            to={`/journal/${this.props.match.params.slug}`}
            title="Your journal's public page"
          >
            Give it a look.
          </Link>
        </span>,
      );
    } catch (e) {
      this.setState({
        error: <span>There was a problem updating the journal details, please try again.</span>,
        isBeingUpdated: false,
      });
    }
  }

  private onHeaderUpload() {
    this.props.onUpdate(
      <span>
        Saved!&nbsp;
        <Link
          to={`/journal/${this.props.match.params.slug}`}
          title="Your journal's public page"
        >
          Give it a look.
        </Link>
      </span>,
    );
  }

  private isReadyForPublication() {
    // TODO Validate name
    return this.state.periodical && this.state.periodical.name && !!this.props.orcid;
  }
  private makePublic(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (!this.state.periodical || !this.isReadyForPublication()) {
      // TODO: The user should not have been able to make the Journal public
      //       if no name is set yet, so log this if it does happen.
      return;
    }
    this.setState({ isBeingMadePublic: true });

    ReactGA.event({
      action: 'Make public',
      category: 'Journal Management',
    });

    // Thanks to `isReadyForPublication` above, we can be sure that state.periodical.name is set.
    // We have to explicitly tell TypeScript, though, hence the `!`.
    const periodicalName: string = this.state.periodical.name !;

    const slug = getSlugForName(periodicalName);

    makePeriodicalPublic(this.props.match.params.slug, slug)
      .then((response) => {
        this.setState({
          hasBeenMadePublic: true,
          isBeingMadePublic: false,
          periodical: response.result,
        });
      })
      // The test for the `catch` results in odd errors that I couldn't solve,
      // so I've disabled that test for now:
      .catch(/* istanbul ignore next */() => {
        this.setState({
          // tslint:disable-next-line:max-line-length
          error: <span>There was a problem making the journal public, please check its configuration and try again.</span>,
          isBeingMadePublic: false,
        });
      });
  }

  private getTodoClassForName() {
    return 'journalProgress__todo '
      + (this.state.periodical && this.state.periodical.name ? 'journalProgress__todo--done' : '');
  }
}

// The next function is trivial (and forced to be correct through the types),
// but testing it requires inspecting the props of the connected component and mocking the store.
// Figuring that out is not worth it, so we just don't test this:
/* istanbul ignore next */
function mapStateToProps(state: AppState): JournalManagementPageStateProps {
  return {
    orcid: state.account.orcid,
  };
}
/* istanbul ignore next */
function mapDispatchToJournalManagementPageProps(
  dispatch: ThunkDispatch<AlertState, {}, CompleteSomethingAction | IgnoreCompleteSomethingAction>) {
  return {
    onUpdate: (successMessage: JSX.Element) => dispatch(completeSomething(successMessage)),
  };
}

export const JournalManagementPage = connect<
  JournalManagementPageStateProps,
  JournalManagementPageDispatchProps,
  JournalManagementPageRouteProps,
  AppState
>(
    mapStateToProps,
    mapDispatchToJournalManagementPageProps,
  )(
    BareJournalManagementPage,
  );
