import { PrerenderableRouter } from '../../src/components/prerenderableRouter/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should match the snapshot when rendering in a browser-like environment', () => {
  const quip = shallow(<PrerenderableRouter/>);

  expect(toJson(quip)).toMatchSnapshot();
});
