import { AlertCalloutProps, BareAlertCallout } from '../../src/components/alertCallout/component';

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

const mockProps: AlertCalloutProps = {
  alerts: {},
};
function initialiseCallout({ alerts = mockProps.alerts }: Partial<AlertCalloutProps> = {}) {
  return <BareAlertCallout alerts={alerts}/>;
}

it('should not render anything when there are no alerts', () => {
  const callout = mount(initialiseCallout({ alerts: {} }));

  expect(callout.find('.message')).not.toExist();
});

it('should display a given error', () => {
  const callout = mount(initialiseCallout({ alerts: { error: 'Some error' } }));

  expect(callout.find('.message').text()).toBe('Some error');
});

it('should mark errors as alerts for screen readers', () => {
  const callout = mount(initialiseCallout({ alerts: { error: 'Arbitrary error' } }));

  expect(callout.find('.message').prop('role')).toBe('alert');
});

it('should display a given error even when there are warnings and success messages', () => {
  const callout = mount(initialiseCallout({
    alerts: { error: 'Some error', warning: 'Arbitrary warning', success: 'Arbitrary success message' },
  }));

  expect(callout.find('.message').text()).toBe('Some error');
});

it('should display a given error message while it is fading out', () => {
  const callout = mount(initialiseCallout({ alerts: { error: 'Some error message' } }));

  expect(callout.find('.message').text()).toBe('Some error message');

  callout.find('Transition.errorTransition').prop('onEnter')();
  callout.setProps({ alerts: { error: undefined } });

  expect(callout.find('.message').text()).toBe('Some error message');
});

it('should display a given warning', () => {
  const callout = mount(initialiseCallout({ alerts: { warning: 'Some warning' } }));

  expect(callout.find('.message').text()).toBe('Some warning');
});

it('should mark warnings as alerts for screen readers', () => {
  const callout = mount(initialiseCallout({ alerts: { warning: 'Arbitrary warning' } }));

  expect(callout.find('.message').prop('role')).toBe('alert');
});

it('should display a given warning even when there are success messages', () => {
  const callout = mount(initialiseCallout({
    alerts: { warning: 'Some warning', success: 'Arbitrary success message' },
  }));

  expect(callout.find('.message').text()).toBe('Some warning');
});

it('should display a given warning message while it is fading out', () => {
  const callout = mount(initialiseCallout({ alerts: { warning: 'Some warning message' } }));

  expect(callout.find('.message').text()).toBe('Some warning message');

  callout.find('Transition.warningTransition').prop('onEnter')();
  callout.setProps({ alerts: { warning: undefined } });

  expect(callout.find('.message').text()).toBe('Some warning message');
});

it('should display a given success message', () => {
  const callout = mount(initialiseCallout({ alerts: { success: 'Some success message' } }));

  expect(callout.find('.message').text()).toBe('Some success message');
});

it('should mark success messages as alerts for screen readers', () => {
  const callout = mount(initialiseCallout({ alerts: { success: 'Arbitrary success message' } }));

  expect(callout.find('.message').prop('role')).toBe('alert');
});

it('should display a given success message while it is fading out', () => {
  const callout = mount(initialiseCallout({ alerts: { success: 'Some success message' } }));

  expect(callout.find('.message').text()).toBe('Some success message');

  callout.find('Transition.successTransition').prop('onEnter')();
  callout.setProps({ alerts: { success: undefined } });

  expect(callout.find('.message').text()).toBe('Some success message');
});
