#!/bin/bash
set -e

# This script should get the root dir of changesets as argument.
# This allows it to work both locally in Docker, and in CI.
if [ -z "$1" ];
then
  echo "Please pass the location of changeset scripts as the first argument.";
  exit 1;
fi

echo "Provisioning database at $PGHOST using changesets in $1."

for d in "$1"/*/; do
  # The basename should be the stack name
  STACK=$(basename "$d");

  if [ ! -d "$d"schemaChanges/ ];
  then
    echo "No changesets for stack \`${STACK}\` - skipping database initialisation.";
    continue;
  fi

  echo "Initialising schema \`${STACK}\`";
  # Create the schema for this stack if it doesn't exist yet (it shouldn't, but to be complete):
  psql --command="CREATE SCHEMA IF NOT EXISTS ${STACK};";

  # Make sure there is a table that tracks which changesets we've executed
  psql << SQL
CREATE TABLE IF NOT EXISTS ${STACK}.schema_changelog (
  version VARCHAR(4) PRIMARY KEY,
  create_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
SQL

  # Check which changesets have already been applied in the past...
  psql --command="SELECT version FROM ${STACK}.schema_changelog;" --tuples-only --no-align > "/tmp/${STACK}_changes";

  # ...then discard those changesets:
  for REDUNDANT_CHANGESET in $(cat "/tmp/${STACK}_changes"); do
    echo "Discarding changeset ${REDUNDANT_CHANGESET} for stack ${STACK}"
    rm "$d"schemaChanges/"${REDUNDANT_CHANGESET}".pgsql;
  done

  # Then apply all remaining changesets:
  for f in "$d"schemaChanges/*; do
    # If no changesets remain, $f will point to the file `*`, so skip $f if it does not exist
    [ -e "$f" ] || continue;

    # Strip the full path...
    filename=$(basename "$f");
    # ...and the extension - what remains should be the version number of the changeset
    CHANGESET="${filename%.*}";
    echo "Applying changeset ${CHANGESET} for stack ${STACK}"
    PGOPTIONS="--search-path=${STACK}" psql --file="$f";
    PGOPTIONS="--search-path=${STACK}" psql --command="INSERT INTO schema_changelog (version) VALUES ('$CHANGESET');";
  done
done