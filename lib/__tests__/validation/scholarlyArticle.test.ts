import {
  scholarlyArticleDescriptionValidators,
  scholarlyArticleNameValidators,
} from '../../validation/scholarlyArticle';

describe('Artcle name', () => {
  it('should not be empty', () => {
    expect(scholarlyArticleNameValidators.map((validator) => validator.id).indexOf('minLength')).not.toBe(-1);
    expect(scholarlyArticleNameValidators.find((validator) => validator.id === 'minLength').options)
      .toEqual({ minLength: 1 });
  });
});
